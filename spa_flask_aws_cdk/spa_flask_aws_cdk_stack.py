from aws_cdk import core as cdk

# For consistency with other languages, `cdk` is the preferred import name for
# the CDK's core module.  The following line also imports it as `core` for use
# with examples from the CDK Developer's Guide, which are in the process of
# being updated to use `cdk`.  You may delete this import if you don't need it.
from aws_cdk import core
from aws_cdk import aws_s3 as s3
from aws_cdk import aws_ecr as ecr
from aws_cdk import aws_certificatemanager as acm
from aws_cdk import aws_lambda as lambda_
from aws_cdk import aws_apigateway as apigateway
import aws_cdk.aws_route53 as route53
import aws_cdk.aws_s3_deployment as s3deploy
import aws_cdk.aws_route53_targets as targets
from aws_cdk import aws_cloudfront_origins as origins
from aws_cdk import aws_cloudfront as cloudfront
import docker
from os import path
from pynpm import NPMPackage

class SpaFlaskAwsCdkStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        ZONE = 'demo.saasment.com'
        DOMAIN_NAME_FRONT = 'cdk-spa.demo.saasment.com'
        DOMAIN_NAME_BACKEND = 'cdk-spa-api.demo.saasment.com'

        
        # Create interface to already existing hosted zone
        root_hosted_zone = route53.HostedZone.from_hosted_zone_attributes(self, 'RootHostedZone',
            hosted_zone_id='Z05422531LZAQAHZETW5S',
            zone_name='saasment.com')

        
        # Create a new zone
        hosted_zone = route53.HostedZone(self, 'HostedZone',
            zone_name=ZONE
        )

        # Create NS record in the new zone
        route53.NsRecord(self, 'HostedZoneNS',
            values=hosted_zone.hosted_zone_name_servers,
            zone=root_hosted_zone,
            record_name=ZONE)

        # Create & validate SSL certificates for frontend & backend domains
        front_certificate = acm.Certificate(self, 'staticWebCert',
            domain_name=DOMAIN_NAME_FRONT,
            validation=acm.CertificateValidation.from_dns(hosted_zone)
        )

        backend_certificate = acm.Certificate(self, 'apiCert',
            domain_name=DOMAIN_NAME_BACKEND,
            validation=acm.CertificateValidation.from_dns(hosted_zone)
        )

        # Create bucket for static pages
        static_bucket = s3.Bucket(self, 'staticWebBucket',
            website_index_document='index.html',
            website_error_document='index.html',
            public_read_access=True
        )

        # Upload nuxt static pages to S3
        s3deploy.BucketDeployment(self, 'staticDeployment', 
            destination_bucket=static_bucket,
            sources=[s3deploy.Source.asset('./frontend',
                bundling=core.BundlingOptions(
                    image = core.BundlingDockerImage.from_registry(image='node:lts'),
                    command = ['bash', '-c', ' && '.join(['ls -la ', 'npm install', 'npm run build', 'npm run generate', 'cp -r /asset-input/dist/* /asset-output/'])]
                )
            )]
        )

        distribution = cloudfront.Distribution(self, 'staticWebBucketDistribution',
            default_behavior={ 
                'origin': origins.S3Origin(static_bucket),
                'viewer_protocol_policy': cloudfront.ViewerProtocolPolicy.REDIRECT_TO_HTTPS

            },
            domain_names=[DOMAIN_NAME_FRONT],
            certificate=front_certificate,
            error_responses=[
                cloudfront.ErrorResponse(http_status=403, response_http_status=200, response_page_path='/index.html'),
                cloudfront.ErrorResponse(http_status=404, response_http_status=200, response_page_path='/index.html'),
            ]
        )

        route53.AaaaRecord(self, 'frontendAlias',
            zone=hosted_zone,
            target=route53.RecordTarget.from_alias(targets.CloudFrontTarget(distribution)),
            record_name=DOMAIN_NAME_FRONT
        )

        backend = lambda_.DockerImageFunction(self, "ECRFunction",
            code=lambda_.DockerImageCode.from_image_asset("./backend")
        )
        
        apigwdomain = apigateway.DomainNameOptions(
            certificate=backend_certificate,
            domain_name=DOMAIN_NAME_BACKEND
        )

        apigw = apigateway.LambdaRestApi(self, "myapi",
            handler=backend,
            domain_name=apigwdomain
        )

        route53.AaaaRecord(self, 'backendAlias',
            zone=hosted_zone,
            target=route53.RecordTarget.from_alias(targets.ApiGateway(apigw)),
            record_name=DOMAIN_NAME_BACKEND
        )
